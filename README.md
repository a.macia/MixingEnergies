MIXING ENERGY AND DATA EXTRACTOR FOR CLUSTERS

This code is intended to calculate properties for clusters.
As it is right now, it calculates Reactive Mixing Energies (RME) and Mixing Energies (ME) 
if the clusters required are available. Otherwise it will print out which possible clusters
are needed. 

Files are:
	-Tablegenerator.py -> generates the csv table 
	-functions.py -> contains the descriptions of the functions to calculate the RME and ME

TODOs:

	-Add capability to select which functions to use
	-Add other structural information, such as A-B distances, angles, etc
	-Change the error messages to be more profitable
	-Check examples

