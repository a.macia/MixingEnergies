"""FUNCTIONS FOR MIXING ENERGIES.

This code is intended to calculate the different energies involved in
the mixing energy.

Two functions calculate the Mixing and Reactive energy, while the
perform_calculations handles the reading of PANDAS dataframe and looks for
the pure clusters on it, and gives warnings if some data is missing.

Mixing Energy: Rel. Energy of cluster against bulk units - Rel. Energy of
clusters of same size * proportion of that cluster
I.E: E[(MgO)20(SiO2)10] - (20/10* E[(MgO)40] + 10/20 E[(SiO2)20])

Reactive Mixing Energy: Rel. Energy of cluster - (Energy of clusters A and B
with the size of the components of the cluster)
I.E: E[(MgO)20(SiO2)10] - (E[(MgO)20] + E[(SiO2)10])
"""

import pandas as pd
import numpy as np


def calculate_mixing_energy(clust_energy, num_mA, num_mB, monA_energy,
                            monB_energy, pure_cenergyA, num_mpA, pure_cenergyB,
                            num_mpB):
    """Calculate Mixing energy of a cluster.

    Variables:
    clust_energy : energy of the cluster
    num_ma/b : number of A/B monomers
    monA/B_energy: energy of A/B monomer in Bulk structure
    pure_cenergyA/B: energy of the pure cluster of same total size as cluster
    num_mpA/B: number of monomers in the pure cluster A/B
    """
    "Calculate stabilization against monomers of bulk."
    bind_energy = clust_energy - (monA_energy * num_mA + monB_energy *
                                  num_mB)
    be_unit = bind_energy/(num_mA + num_mB)

    """Calculate stabilization agains pure clusters of same size."""
    cbe = (pure_cenergyA * num_mA/num_mpA + pure_cenergyB * num_mB/num_mpB -
           (num_mA * monA_energy + num_mB * monB_energy)) / (num_mA + num_mB)

    """Calculate mixing energy."""
    mixing_energy = be_unit - cbe
    return cbe, be_unit, mixing_energy


def calculate_reactive_mixing_energy(clust_energy,
                                     num_mA, num_mB,
                                     isolated_cenergyA,
                                     isolated_cenergyB):
    """Calculate stabilization against the clusters."""
    rme = ((clust_energy - (isolated_cenergyA + isolated_cenergyB)) / (num_mA +
           num_mB))
    return rme

def perform_calculations(dataframe,
                         en_monoA,
                         en_monoB,
                         mon_ratio,
                         Ecol='totenergy',
                         monAcol='SiO2units',
                         monBcol='MgOunits',
                         tablefile='finaltable.csv'):
    """Calculate all bining energies using dataframe.

    Variables:
    dataframe: dataframe which contains the data
    en_monoA/B: energy of monomer A/B in bulk
    mon_ratio: ratio among monomers to compute extremes (ie: 2Mg = 1 Si )
    Ecol : Name of the column which contains the energy in the dataframe
    monA/Bcol : name of the column which contains the number of monomers A/B
    tablefile : name of the output table which will contain the extra energies
    """
    warnings = ''
    expanded_dataframe = dataframe.copy()
    expanded_dataframe['MixingEnergy'] = np.nan
    expanded_dataframe['ReactiveMixingEnergy'] = np.nan
    for index, row in dataframe.iterrows():
        max_monA = row[monAcol] + row[monBcol] * mon_ratio
        max_monB = row[monBcol] + row[monAcol] / mon_ratio

        'Checking the presence of extremes and calculating BE'
        max_monA_row = (expanded_dataframe
                        [(expanded_dataframe[monAcol] == max_monA) &
                        (expanded_dataframe[monBcol] == 0)])
        max_monB_row = (expanded_dataframe
                        [(expanded_dataframe[monAcol] == 0) &
                        (expanded_dataframe[monBcol] == max_monB)])
        if max_monA_row.empty or max_monB_row.empty:
            warnings += "{}\n".format(row.iloc[0])
            warnings += """The extreme values SiO2 {} MgO 0
or SiO2 0 MgO {}
weren't found unable to calculate Mixing for\n""".format(max_monA, max_monB)
        else:
            results = calculate_mixing_energy(row[Ecol], row[monAcol],
                                  row[monBcol],
                                  en_monoA,
                                  en_monoB,
                                  max_monA_row[Ecol].values[0],
                                  max_monA,
                                  max_monB_row[Ecol].values[0],
                                  max_monB
                                  )
            expanded_dataframe['MixingEnergy'].values[index] = results[2]

        'Checking the presence of extremes and calculating BE'
        if row[monAcol] == 0 or row[monBcol] == 0:
            results = calculate_reactive_mixing_energy(row[Ecol],
                                  row[monAcol],
                                  row[monBcol],
                                  0.0,
                                  0.0,
                                  )
            expanded_dataframe.set_value(index,'ReactiveMixingEnergy', results)

        else:
            pclust_monA_row = (expanded_dataframe
                            [(expanded_dataframe[monAcol] == row[monAcol]) &
                            (expanded_dataframe[monBcol] == 0)])
            pclust_monB_row = (expanded_dataframe
                            [(expanded_dataframe[monAcol] == 0) &
                            (expanded_dataframe[monBcol] == row[monBcol])])



    #### OLD PART
            if pclust_monA_row.empty or pclust_monB_row.empty:
                warnings += "{}\n".format(row.iloc[0])
                warnings += """The clusters SiO2 {} MgO 0
    or SiO2 0 MgO {}
    weren't found, unable to calculate reactive mixing energy\n""".format(row[monAcol], row[monBcol])
            else:
                results = calculate_reactive_mixing_energy(row[Ecol],
                                      row[monAcol],
                                      row[monBcol],
                                      pclust_monA_row[Ecol].values[0],
                                      pclust_monB_row[Ecol].values[0],
                                      )
                expanded_dataframe.set_value(index,'ReactiveMixingEnergy', results)




#    expanded_dataframe.to_csv(tablefile)

    return warnings, expanded_dataframe
