from ase.io import read
import functions
import pandas as pd
import sys
import re

def name_cleaning(string_name):
    elements = {}
    for part in re.findall(r'[A-Z][^A-Z]*', string_name):
        element = re.findall("[A-Z][a-z]*", part)
        proportion = re.findall("[0-9]{1,}", part)
        if len(proportion):
            num = int(proportion[0])
        else:
            num = 1
        elements[element[0]] = num
    return elements

# Dataframe creation
monomerA = 'SiO2'  # INPUT NAME OF THE MONOMERS
monomerB = 'MgO'   # INPUT NAME OF THE MONOMERS
monA_constituents = name_cleaning(monomerA)
monB_constituents = name_cleaning(monomerB)
unique_A = list(set(monA_constituents) - set(monB_constituents))
unique_B = list(set(monB_constituents) - set(monA_constituents))
columns_name = ['file', 'Energy(eV)', monomerA, monomerB, 'Mixing']
result_table = pd.DataFrame(columns=columns_name)


# Reading outputs
for fout in sys.argv[1:]:
    molecule = read(fout)
    num_monA = molecule.get_chemical_symbols().count(unique_A[0])
    num_monB = molecule.get_chemical_symbols().count(unique_B[0])
    mixing = float(num_monB) / float(num_monA + num_monB)
    row_toadd = pd.DataFrame([[fout, molecule.get_potential_energy(),
                               num_monA, num_monB, mixing]],
                               columns=columns_name)
    result_table = result_table.append([row_toadd], ignore_index=True)


print(result_table)

warnings, result_table_final = functions.perform_calculations(result_table,
                                                              -11997.082857407,
                                                              -7500.476649459,
                                                              0.5,
                                                              Ecol='Energy(eV)',
                                                              monAcol=monomerA,
                                                              monBcol=monomerB)
result_table_final.to_csv('Final.csv')
lfile = open('table_generation.log', 'w')
lfile.write(warnings)
lfile.close()
